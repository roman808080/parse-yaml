import unittest
from parse_yaml import parse_yaml


class ParseYamlTests(unittest.TestCase):
    def test_empty_string(self):
        self.assertEqual(parse_yaml(''), {})

    def test_parse_simple_dictionary(self):
        self.assertEqual(parse_yaml('key: value'), {'key': 'value'})

    def test_parse_dictionary_with_few_keys(self):
        self.assertEqual(parse_yaml('key1: value1\n'
                                    'key2: value2\n'
                                    'key3: value3'), {'key1': 'value1', 'key2': 'value2', 'key3': 'value3'})

    def test_parse_dictionary_inside_dictionary(self):
        self.assertEqual(parse_yaml('key1:\n'
                                    '  key2: value2'), {'key1': {'key2': 'value2'}})
        self.assertDictEqual(parse_yaml('key1:\n'
                                        '  key2: value2\n'
                                        '  key3: value3\n'
                                        'key4: value4'), {'key1': {'key2': 'value2', 'key3': 'value3'},
                                                          'key4': 'value4'})

    def test_parse_deep_dictionary(self):
        self.assertDictEqual(parse_yaml('key1:\n'
                                        '  key2:\n'
                                        '    key3: value3'), {'key1': {'key2': {'key3': 'value3'}}})
        self.assertDictEqual(parse_yaml('key1:\n'
                                        '  key2:\n'
                                        '    key3: value3\n'
                                        '  key4: value4\n'
                                        'key5: value5'), {'key1': {'key2': {'key3': 'value3'}, 'key4': 'value4'},
                                                          'key5': 'value5'})

    def test_simple_list(self):
        self.assertEqual(parse_yaml('- value1'), ['value1'])

    def test_two_value_in_list(self):
        self.assertEqual(parse_yaml('- value1\n'
                                    '- value2'), ['value1', 'value2'])

    def test_list_inside_dict(self):
        self.assertEqual(parse_yaml('key1:\n'
                                    '  - value1\n'
                                    '  - value2'), {'key1': ['value1', 'value2']})

    def test_dict_inside_list(self):
        self.assertEqual(parse_yaml('- key1: value1'), [{'key1': 'value1'}])
