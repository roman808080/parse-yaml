def count_spaces(string):
    count = 0

    for letter in string:
        if letter == ' ':
            count += 1
            continue

        return count


def return_last_index(current_spaces, strings, index):
    for string in strings[index:]:
        if count_spaces(string) == current_spaces:
            return index

        index += 1

    return index


def yaml_to_dict(yaml_strings):
    dictionary = {}

    i = 0
    while i < len(yaml_strings):
        parts = yaml_strings[i].split(':')

        if len(parts) == 1 and '-' in parts[0]:
            return yaml_to_list(yaml_strings)
        elif len(parts) == 1:
            return parts[0]
        elif parts[1] == '':
            current_spaces = count_spaces(parts[0])
            i += 1

            last_index = return_last_index(current_spaces, yaml_strings, i)
            dictionary[parts[0].strip()] = yaml_to_dict(yaml_strings[i: last_index])
            i = last_index
        else:
            dictionary[parts[0].strip()] = parts[1].strip()
            i += 1

    return dictionary


def yaml_to_list(yaml_strings):
    yaml_list = []

    i = 0
    while i < len(yaml_strings):
        item = yaml_strings[i].replace('-', '').strip()
        yaml_list.append(yaml_to_dict([item]))
        i += 1

    return yaml_list


def parse_yaml(yaml_string):
    if yaml_string == '':
        return {}

    yaml_strings = yaml_string.split('\n')
    if yaml_string[0] == '-':
        return yaml_to_list(yaml_strings)
    return yaml_to_dict(yaml_strings)
